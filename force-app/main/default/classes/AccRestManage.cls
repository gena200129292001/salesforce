@RestResource(urlMapping='/Accounts/*')
global with sharing class AccRestManage {
  
    @HttpPost
  global static list <Account> Acc() {
        RestRequest AReq = RestContext.request;
        String Body = AReq.requestBody.toString();      
        List<Account> ThisAcc = (List<Account>) JSON.deserialize(Body, List<Account>.class);
        for(Account a : ThisAcc){
            if (a.AccountSource!=null){
           a.OriginalSource__c = a.AccountSource;           
}
  a.AccountSource = 'External account';
          
}


insert ThisAcc;
return ThisAcc;
}         
 
}