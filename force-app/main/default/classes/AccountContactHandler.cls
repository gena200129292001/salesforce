public class AccountContactHandler extends TriggerHandler{
    public list<AccountContact__c> newAC = (list<AccountContact__c>) Trigger.new;
    public static Map<Id, AccountContact__c> oldACMap = (Map<Id, AccountContact__c>) Trigger.oldMap; 
        
    public override void afterInsert() {
        AccountContactService.AfterInsert(this.newAC);
     

}
    public override void afterUpdate() {
    
            
}
}