public class AsyncJo {
    @AuraEnabled(cacheable=true)
    public static List<AsyncApexJob> GetApexJobs(){
       
           return [ select ApexClassId, ApexClass.name, Id, JobItemsProcessed, JobType, Status, NumberOfErrors, MethodName,CreatedById, CompletedDate
            from AsyncApexJob
            where JobType = 'BatchApex' and CompletedDate>2021-08-24T10:10:00.000Z];

        }
@AuraEnabled(cacheable=true)
public static Object reRunBatch(String name) {
    Type typ = Type.forName(name);
    Object instance = null;
        instance = typ.newInstance();
Database.executeBatch ((Database.Batchable <sObject>) instance);
return instance;
}    
@AuraEnabled(cacheable=true)
public static void AbortBatch(Id JobId) {
    System.abortJob(JobId);
    
}    
}