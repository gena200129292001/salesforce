public class Logger {
    public static void log(Exception ex, Database.BatchableContext context) {
     string message = ex.getMessage();   
     string Trace = ex.getStackTraceString();
List<AsyncApexJob> apexJob = [SELECT 
                              CreatedBy.Name,
                              ApexClassId,
                              ApexClass.Name 
                              
                                      
                              FROM AsyncApexJob
                                     
                              WHERE Id = :context.getJobId()];
        
        Logger__e EventLogErr = new Logger__e(
           EvContextId__c=apexJob[0].ApexClassId,
           EvContextName__c=apexJob[0].ApexClass.Name,
           LogLavel__c='ERROR',
           EvMessage__c=message,
           EvStackTrace__c=Trace,
           EvUserName__c=apexJob[0].CreatedBy.Name   
                          
        );
 Database.SaveResult sr = EventBus.publish(EventLogErr); 
        
    }
    public static void log(String Name){
         Logger__e EventLogSucc = new Logger__e(        
           EvContextName__c=Name,
           LogLavel__c='INFO',
           EvMessage__c='batch finished successfully'                  
        );
 Database.SaveResult srr = EventBus.publish(EventLogSucc); 
        
    }
}