public with sharing class PrimHandler {
    
    public static void AutoPrimFrst(List<AccountContact__c> UnList){    
List<AccountContact__c> All = [select Id from AccountContact__c where Contact__c = :UnList[0].Contact__c ORDER BY CreatedDate desc];
       if (All.size() == 0){
           UnList[0].IsPrimary__c = true;}
        
        if (All.size()>0 && UnList[0].IsPrimary__c == true){
         for (AccountContact__c F : All){
             F.IsPrimary__c = false;  
         }          
     }
     else if (All.size()>0 && UnList[0].IsPrimary__c == false){          
              All[0].IsPrimary__c = true;  
          } 
        update All;
       } 
    

      
public static void AutoPrim(List<AccountContact__c> UnList){ 
List<AccountContact__c> All = [select Id from AccountContact__c where Contact__c = :UnList[0].Contact__c ORDER BY CreatedDate desc];
     if (UnList[0].IsPrimary__c == true){
         for (AccountContact__c F : All){
             F.IsPrimary__c = false;  
         }          
     }
     else if (UnList[0].IsPrimary__c == false){          
              All[0].IsPrimary__c = true;  
          }
           update All; 

       } 
    }