public class ReturnLogsToLwc {
     @AuraEnabled(cacheable=true)
    public static List<Log__c> GetLogs(){
       
           return [ select message__c, Stack_trace_String__c, Id, Name
            from Log__c
           ];

        }

}