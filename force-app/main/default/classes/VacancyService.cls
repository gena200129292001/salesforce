public class VacancyService {
 
    
    
      public static void ShareRead(Id recordId, Id userOrGroupIdD){
      Vacancy__Share VacShr  = new Vacancy__Share();
      VacShr.ParentId = recordId;
      VacShr.UserOrGroupId = userOrGroupIdD;
      VacShr.AccessLevel = 'Read';       
      VacShr.RowCause = Schema.Vacancy__Share.RowCause.Because__c;
      Database.SaveResult sr = Database.insert(VacShr,false);
      }
  //  is null!!!
    public static void InsertSharing(){
        list<Manager__c> Man = [select name, User__c from Manager__c];
 Map<id, Manager__c> Managers = new Map<id, Manager__c>(Man);
        for(Id idd : VacancyHandler.newVacMap.keySet()){
        if(VacancyHandler.newVacMap.get(idd).Manager__c != null){
        Id f = VacancyHandler.newVacMap.get(idd).Manager__c; 
        VacancyService.ShareRead(idd, Managers.get(f).User__c);
    }
}
}
        public static void UpdateSharing(){
            list<Manager__c> Man = [select name, User__c from Manager__c];
 Map<id, Manager__c> Managers = new Map<id, Manager__c>(Man);
              
        for (Id newRecord : VacancyHandler.newVacMap.keySet())
{
    if (VacancyHandler.newVacMap.get(newRecord).Manager__c != VacancyHandler.oldVacMap.get(newRecord).Manager__c){
      Id f = VacancyHandler.newVacMap.get(newRecord).Manager__c;
      Id o = VacancyHandler.oldVacMap.get(newRecord).Manager__c;
      Id old = Managers.get(o).User__c;
      VacancyService.ShareRead(newRecord, Managers.get(f).User__c);
        
    
      //list<Vacancy__Share> c = [select Id from  Vacancy__Share where UserOrGroupId = :old and ParentId in VacancyHandler.oldVacMap.keySet() and rowcase];
        list<Vacancy__Share> c = [select Id from  Vacancy__Share where UserOrGroupId = :old]; 
      Database.deleteResult[] Llist = Database.delete(c, false);
        
        
        
}
}        
            
        }
}