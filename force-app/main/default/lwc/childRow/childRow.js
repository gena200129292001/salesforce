import { LightningElement, api, wire, track } from 'lwc';
import reRunBatch from "@salesforce/apex/AsyncJo.reRunBatch";
import GetLogs from "@salesforce/apex/ReturnLogsToLwc.GetLogs";
export default class ChildRow extends LightningElement {
    @api row;
    @api logrow;
    logsData =[];
    handleClick(event) {
     if(event.target.label === 'Run'){
        reRunBatch({
            name: this.row.className
        })
   }
   if(event.target.label === 'Abort'){
    AbortBatch({
        JobId: this.row.id
    })
    }
}

@track openModal = false;
    showModal(event) {
        if(event.target.title === 'Failed'){
        this.openModal = true;
    }
}
    closeModal() {
        this.openModal = false;
    }
    @wire(GetLogs)
getLogsList(response) {
  let datalog = response.data;
  let error = response.error;
  

  if (datalog || error) {
    this.processing = false;
  }

  if (datalog) {
    this.logsData = [];

    datalog.forEach((item) => {
      this.logsData.push({
        id: item.Id,
        message: item.Message__c,
        stackTrace: item.Stack_trace_String__c,
        name: item.Name
      });
    });
  } else if (error) {
    console.log("error");
  }
}   
}