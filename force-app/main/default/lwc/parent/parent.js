import { LightningElement, wire} from 'lwc';
import GetApexJobs from "@salesforce/apex/AsyncJo.GetApexJobs";
export default class Parent extends LightningElement {

apexJobData = [];
@wire(GetApexJobs)
getApexJobList(response) {
  let data = response.data;
  let error = response.error;
  if (data || error) {
    this.processing = false;
  }

  if (data) {
    this.apexJobData = [];

    data.forEach((item) => {
        var Button = '';
        if (item.Status === 'Completed' || 'Failed'){
        Button='Run';        
        }
        if (item.Status === 'Aborted'){
            Button='reRun';        
            }
        if (item.Status === 'Processing'){
                Button='Abort';        
                }    
      this.apexJobData.push({
        id: item.Id,
        className: Object.keys(item).includes("ApexClass")
          ? item.ApexClass.Name
          : "",
        jobItemsProcessed: item.JobItemsProcessed,
        jobType: item.JobType,
        status: item.Status,
        numberOfErrors: item.NumberOfErrors,
        methodName: item.MethodName,
        completedDate: item.CompletedDate,
        buttonLabel: Button
      });
    });
  } else if (error) {
    console.log("error");
  }
}   

}