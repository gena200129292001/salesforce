trigger AccountContact on AccountContact__c (before insert, after update) {
    new AccountContactHandler().run();
}