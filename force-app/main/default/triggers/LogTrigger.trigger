trigger LogTrigger on Logger__e (after insert) {
    
    list<Log__c> AllLogs = new List<Log__c>();
    for (Logger__e  le : trigger.new){
        if (le.LogLavel__c == 'INFO') {
            Log__c LogSucc = new Log__c(
 Message__c='batch finished successfully',
 Logging_Level__c=le.LogLavel__c,
 Context_Name__c='batch<<'+le.EvContextName__c+'>>'
);
AllLogs.add(LogSucc);
        }else{Log__c LogErr = new Log__c(
Message__c=le.EvMessage__c,
Logging_Level__c=le.LogLavel__c,
Context_Name__c=le.EvContextName__c,
User_Name__c=le.EvUserName__c, 
Stack_Trace_String__c=le.EvStackTrace__c,
Context_Id__c=le.EvContextId__c           
);            
AllLogs.add(LogErr);            
}

}

    insert AllLogs;
}