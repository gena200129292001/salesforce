trigger VacancyTrigger on Vacancy__c (after insert, after update) {
    new VacancyHandler().run();

}